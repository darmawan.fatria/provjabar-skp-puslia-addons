from openerp.osv import fields, osv

class app_skp_puslia_settings(osv.osv_memory):
    _name = 'app.skp.puslia.settings'
    _description = 'Konfigurasi Aplikasi SKP Dan puslia'

    _columns = {
            'name' : fields.char('Konfigurasi Integrasi SKP puslia', size=64, readonly=True),
            'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     ),
            'target_period_year'     : fields.char('Periode Tahun', size=4, ),
    }
    def action_validate_to_project_skp_format_scheduler(self, cr, uid, context=None):

        data_input_pool = self.pool.get('project.skp.puslia.data.integration')
        #for param_obj in self.browse(cr, uid, ids, context=context):
        data_ids        = data_input_pool.search(cr, uid, [#('state', 'not in', ('synced','done')),
                                               ('periode_tahun', '=', '2015'), 
                                               ('periode_bulan', '=', '10'),
                                               ('notes','=','to_be_sync'),
#                                               ('state','=','new')
                                               ], 
                                    context=None,limit=100)
        if data_ids:
                print "data yang akan sinkron : ",len(data_ids)
                data_input_pool.sync_with_project_skp(cr,uid,data_ids,context=None)
        
        return {}
    def action_validate_to_project_skp_format_scheduler_last(self, cr, uid, context=None):

        data_input_pool = self.pool.get('project.skp.puslia.data.integration')
        #for param_obj in self.browse(cr, uid, ids, context=context):
        data_ids        = data_input_pool.search(cr, uid, [('state', 'not in', ('synced','done')),
                                               ('periode_tahun', '=', '2015'),
                                               ('periode_bulan', '=', '10'),
       #                                        ('notes','=','to_be_sync'),
#                                               ('state','=','new')
                                               ], 
                                    context=None,limit=100)
        if data_ids:
                print "data yang akan sinkron : ",len(data_ids)
                data_input_pool.sync_with_project_skp(cr,uid,data_ids,context=None)
        
        return {}

    # -------------------- start for settings
    def action_validate_to_project_skp_format(self, cr, uid, ids, context={}):

        data_input_pool = self.pool.get('project.skp.puslia.data.integration')
        for param_obj in self.browse(cr, uid, ids, context=context):
                data_ids        = data_input_pool.search(cr, uid, [('state', 'not in', ('synced','done')),
                                               ('periode_tahun', '=', param_obj.target_period_year), 
                                               ('periode_bulan', '=', param_obj.target_period_month),
                                               ],
                                    context=None,limit=100)
                if data_ids:
                    print "data yang akan sinkron : ",len(data_ids)
                data_input_pool.sync_with_project_skp(cr,uid,data_ids,context=None)
        
        return {}
    def action_replace_data_project_skp_with_puslia(self, cr, uid, ids, context={}):
        
        data_extraction_pool = self.pool.get('project.skp.puslia.extraction')
        for param_obj in self.browse(cr, uid, ids, context=context):
                data_ids        = data_extraction_pool.search(cr, uid, [('state', '=', 'new'),
                                               ('target_period_year', '=', param_obj.target_period_year), 
                                               ('target_period_month', '=', param_obj.target_period_month)], 
                                    context=None)
                if data_ids:
                    print "data yang akan sinkron : ",len(data_ids)
                data_extraction_pool.sync_and_update_with_project_skp(cr,uid,data_ids,context=None)
        return True
    def action_rollback_project_skp_with_puslia(self, cr, uid, ids, context={}):
        
        data_extraction_pool = self.pool.get('project.skp.puslia.extraction')
        for param_obj in self.browse(cr, uid, ids, context=context):
                data_ids        = data_extraction_pool.search(cr, uid, [('state', '=', 'synced'),
                                               ('target_period_year', '=', param_obj.target_period_year), 
                                               ('target_period_month', '=', param_obj.target_period_month)], 
                                    context=None)
                if data_ids:
                    print "data yang akan dirollback sinkron : ",len(data_ids)
                data_extraction_pool.rollback_sync_with_project_skp(cr,uid,data_ids,context=None)
        return True
app_skp_puslia_settings()