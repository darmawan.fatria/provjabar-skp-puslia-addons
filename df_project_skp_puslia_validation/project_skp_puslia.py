from openerp.osv import fields, osv
from mx import DateTime
import openerp.addons.decimal_precision as dp

class project_skp_puslia_data_integration(osv.Model):
    _name = "project.skp.puslia.data.integration"
    _description='Integrasi Data SKP - puslia'
    def _get_kode_rekening_skp(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for data_integration in self.browse(cr, uid, ids, context=context):
            result = ''
            prefix=data_integration.kode_fungsi+'.'+data_integration.kode_urusan+'.'
            
            if data_integration.kode_opd and  data_integration.kode_program :
                result=prefix+data_integration.kode_opd + '.' +data_integration.kode_program
            if data_integration.kode_opd and  data_integration.kode_program  and  data_integration.kode_kegiatan :
                result=prefix+data_integration.kode_opd + '.' +data_integration.kode_program +'.'+data_integration.kode_kegiatan 
           
            res[data_integration.id] = result
        return res
    _columns = {
        'kode_fungsi' :fields.char('Kode Fungsi',size=1),
        'kode_urusan' :fields.char('Kode Urusan',size=2),
        'kode_opd' :fields.char('Kode OPD',size=2),
        'kode_program' :fields.char('Kode Program',size=3),
        'kode_kegiatan' :fields.char('Kode Kegiatan',size=2),
        'periode_tahun' :fields.char('Periode Tahun',size=4,required=True),
        'periode_bulan' :fields.char('Periode Bulan',size=2,required=True),
        'kode_rekening' :fields.char('Kode Rekening',size=25,required=True),
        #'kode_rekening_skp' :fields.function(_get_kode_rekening_skp, method=True, string='Kode Rekening SKP', type='char', readonly=True,store=True),
        'target_biaya': fields.float('Target Biaya', ),
        'realisasi_biaya': fields.float('Realisasi Biaya',required=True ),
        'realisasi_jumlah_kuantitas_output': fields.float('Realisasi Jumlah Kuantitas Output', required=True,digits_compute=dp.get_precision('no_digit')),
        'tgl_pelaporan' :fields.date('Tanggal Pelaporan',),
        'tgl_data_masuk' :fields.date('Tanggal Data Masuk',),
        'tgl_pengesahan' :fields.date('Tanggal Pengesahan',),

        'nip_penanggung_jawab' :fields.char('NIP Penanggung Jawab',size=20,),
        'nama_penanggung_jawab' :fields.char('Nama Penanggung Jawab',size=75,),
        
        'state': fields.selection([     ('new', 'New'),
                                        ('skp', 'SKP'), 
                                        ('synced', 'Synced'),
                                        ('failed', 'Failed'),
                                        ('done', 'Done'),
                                                      ],
                                        'Status',
                                                     ),
        'notes': fields.text('Catatan' ),
        'tgl_sinkron_puslia' :fields.date('Tanggal Sinkornisasi puslia',),
        'tgl_sinkron_skp' :fields.date('Tanggal Sinkornisasi SKP',),
        
    }
    _defaults = {
        'state': 'new',
    }
    def _old_sync_with_project_skp(self, cr, uid, ids,context=None):
        data_extraction_pool = self.pool.get('project.skp.puslia.extraction')
        project_skp_pool = self.pool.get('project.skp')
        sync_ids = []
        no_task_failed_sync_ids = []
        duplicate_task_failed_sync_ids = []
        for data_integration in self.browse(cr, uid, ids, context=context):
            code_result_messages=''
            data_extraction =  {}
            task_ids = project_skp_pool.search(cr, uid, [('rekening_code', '=', data_integration.kode_rekening),
                                                         ('target_period_year', '=', data_integration.periode_tahun), ('target_period_month', '=', data_integration.periode_bulan)
                            ,('is_puslia_sync','=',False)],
                                    context=None)
            if not task_ids:
                code_result_messages='Tidak Ada Kode Kegiatan Di Periode Bulan Ini'
                no_task_failed_sync_ids.append(data_integration.id)
            elif task_ids and len(task_ids)>2:
                code_result_messages='Ada Lebih Dari 2 Kode Kegiatan Di Periode Bulan Ini'
                duplicate_task_failed_sync_ids.append(data_integration.id)
            else:
                code_result_messages='Sukses'
                for task_obj in project_skp_pool.browse(cr,uid,task_ids,context=None) :
                    data_extraction.update({
                                'name':task_obj.name,
                                'code':data_integration.kode_rekening,
                                'target_period_month':task_obj.target_period_month,
                                'target_period_year':task_obj.target_period_year,
                                'project_skp_id':task_obj.id,
                                'company_id':task_obj.company_id.id,
                                'user_id':task_obj.user_id.id,
                                'notes':code_result_messages,
                                'target_biaya':data_integration.target_biaya,
                                'realisasi_biaya':data_integration.realisasi_biaya,
                                'project_skp_puslia_id':data_integration.id,
                                'state':'new'
                                })
                    extract_id = data_extraction_pool.create(cr,uid,data_extraction,context=None)
                    sync_ids.append(data_integration.id)
            #print "code_result_messages : ",code_result_messages
        now=DateTime.today();
        #print "data yang berhasil : ",sync_ids
        #print "data yang gagal : ",no_task_failed_sync_ids
        #print "data yang gagak : ",duplicate_task_failed_sync_ids
        #sync_success
        if sync_ids:
            self.write(cr, uid, sync_ids, {'state':'synced',
                                      'notes':'success',
                                      'tgl_sinkron_skp':now,
                                      }, context=None) 
        #failed
        if no_task_failed_sync_ids:
            self.write(cr, uid, no_task_failed_sync_ids, {'state':'failed',
                                  'notes':'Tidak Ada Kode Kegiatan Di Periode Bulan Ini',
                                  }, context=None)
        if duplicate_task_failed_sync_ids:
            self.write(cr, uid, duplicate_task_failed_sync_ids, {'state':'failed',
                                      'notes':'Ada Lebih Dari 1 Kode Kegiatan Di Periode Bulan Ini',
                                      }, context=None)      
        return True

    def sync_with_project_skp(self, cr, uid, ids,context=None):
        data_extraction_pool = self.pool.get('project.skp.puslia.extraction')
        project_skp_pool = self.pool.get('project.skp')
        sync_ids = []
        no_task_failed_sync_ids = []
        duplicate_task_failed_sync_ids = []
        domain = [];

        for data_integration in self.browse(cr, uid, ids, context=context):
            code_result_messages=''
            data_extraction =  {}

            task_ids = project_skp_pool.search(cr, uid, [('rekening_code', '=', data_integration.kode_rekening),
                                                       ('target_period_year', '=', data_integration.periode_tahun), ('target_period_month', '=', data_integration.periode_bulan)
							,('is_puslia_sync','=',False)],
                                   context=None)

            print "Data Found : ",data_integration.kode_rekening,' - ',task_ids
            if not task_ids:
                code_result_messages='Tidak Ada Kode Kegiatan Di Periode Bulan Ini'
                no_task_failed_sync_ids.append(data_integration.id)
            elif task_ids and len(task_ids)>4:
                code_result_messages='Ada Lebih Dari 4 Kode Kegiatan Di Periode Bulan Ini'
                duplicate_task_failed_sync_ids.append(data_integration.id)
            else:
                code_result_messages='Sukses'
                for task_obj in project_skp_pool.browse(cr,uid,task_ids,context=None) :
                    data_extraction.update({
                                'name':task_obj.name,
                                'code':data_integration.kode_rekening,
                                'target_period_month':task_obj.target_period_month,
                                'target_period_year':task_obj.target_period_year,
                                'project_skp_id':task_obj.id,
                                'company_id':task_obj.company_id.id,
                                'user_id':task_obj.user_id.id,
                                'notes':code_result_messages,
                                'realisasi_biaya':data_integration.realisasi_biaya,
                                 'realisasi_jumlah_kuantitas_output':data_integration.realisasi_jumlah_kuantitas_output,
                                'project_skp_puslia_id':data_integration.id,
                                'state':'new'
                                })
                    extract_id = data_extraction_pool.create(cr,uid,data_extraction,context=None)
                    sync_ids.append(data_integration.id)
            #print "code_result_messages : ",code_result_messages
        now=DateTime.today();
        #print "data yang berhasil : ",sync_ids
        #print "data yang gagal : ",no_task_failed_sync_ids
        #print "data yang gagak : ",duplicate_task_failed_sync_ids
        #sync_success
        if sync_ids:
            self.write(cr, uid, sync_ids, {'state':'synced',
                                      'notes':'success',
                                      'tgl_sinkron_skp':now,
                                      }, context=None) 
        #failed
        if no_task_failed_sync_ids:
            self.write(cr, uid, no_task_failed_sync_ids, {'state':'failed',
                                  'notes':'Tidak Ada Kode Kegiatan Di Periode Bulan Ini',
                                  }, context=None)
        if duplicate_task_failed_sync_ids:
            self.write(cr, uid, duplicate_task_failed_sync_ids, {'state':'failed',
                                      'notes':'Ada Lebih Dari 1 Kode Kegiatan Di Periode Bulan Ini',
                                      }, context=None)      
        return True
project_skp_puslia_data_integration()
class project_skp_puslia_extraction(osv.Model):
    _name = 'project.skp.puslia.extraction'
    _description='Extract Data SKP - puslia'
    _columns = {
        'name': fields.char('Nama Kegiatan', size=500, readonly=True),
        'code'     : fields.char('Kode Kegiatan', size=25, readonly=True),
        'target_period_month'     : fields.selection([('01', 'Januari'), ('02', 'Februari'),
                                                      ('03', 'Maret'), ('04', 'April'),
                                                      ('05', 'Mei'), ('06', 'Juni'),
                                                      ('07', 'Juli'), ('08', 'Agustus'),
                                                      ('09', 'September'), ('10', 'Oktober'),
                                                      ('11', 'November'), ('12', 'Desember')], 'Periode Bulan'
                                                     , readonly=True),
        'target_period_year'     : fields.char('Periode Tahun', size=4, readonly=True),
        'project_skp_id': fields.many2one('project.skp', 'Realisasi SKP', required=True, readonly=True),
        'project_skp_puslia_id': fields.many2one('project.skp.puslia.data.integration', 'Integrasi Data SKP', readonly=True),
        'company_id': fields.many2one('res.company', 'OPD', readonly=True),
        'user_id': fields.many2one('res.users', 'Pegawai Yang Dinilai', readonly=True),
        'realisasi_biaya'     : fields.float('Biaya', readonly=True,),
        'realisasi_jumlah_kuantitas_output': fields.float('Realisasi Jumlah Kuantitas Output', readonly=True,
                                                          digits_compute=dp.get_precision('no_digit')),

        'state': fields.selection([     ('new', 'Baru'), 
                                        ('synced', 'Synced'),
                                  ],
                                        'Status',
                                                     ),
        'notes': fields.text('Catatan' , readonly=True),
    }
    def sync_and_update_with_project_skp(self, cr, uid, ids,context=None):
        project_skp_pool = self.pool.get('project.skp')
        data_input_pool = self.pool.get('project.skp.puslia.data.integration')
        now=DateTime.today();
        extraction_ids = []
        integration_ids = []
        for data_extraction in self.browse(cr, uid, ids, context=context):
            task =  {}
            task.update({
                                    'realisasi_biaya':data_extraction.realisasi_biaya,
                                    'realisasi_jumlah_kuantitas_output': data_extraction.realisasi_jumlah_kuantitas_output,
                                    'realisasi_skp_puslia_notsame':data_extraction.realisasi_biaya != data_extraction.project_skp_id.realisasi_biaya,
                                    'puslia_sync_date':now,
                                    'is_puslia_sync':True,
                                    'puslia_sync_state':'synced',
                                    
                        })
            if not data_extraction.project_skp_id.puslia_sync_date:
                task.update({
                    'old_realisasi_biaya':data_extraction.project_skp_id.realisasi_biaya,
                    'old_realisasi_jumlah_kuantitas_output': data_extraction.project_skp_id.realisasi_jumlah_kuantitas_output,
                    })
            project_skp_pool.write(cr,uid,data_extraction.project_skp_id.id,task,context=None)
            extraction_ids.append(data_extraction.id)
            integration_ids.append(data_extraction.project_skp_puslia_id.id)
        if extraction_ids:
            self.write(cr, uid, extraction_ids, {'state':'synced'}, context=None)      
        if integration_ids:
            data_input_pool.write(cr, uid, integration_ids, {'state':'done'}, context=None)      
            
        return True
    def sync_and_update_with_project_skp_by_task_id(self, cr, uid, task_id,context=None):
        project_skp_pool = self.pool.get('project.skp')
        data_input_pool = self.pool.get('project.skp.puslia.data.integration')
        now=DateTime.today();
        extraction_ids = []
        integration_ids = []
        list_of_data_extraction = self.search(cr,uid, [('project_skp_id', '=', task_id),], context=None)
        for data_extraction in self.browse(cr, uid, list_of_data_extraction, context=context):
            task =  {}
            
            task.update({
                                    'realisasi_biaya':data_extraction.realisasi_biaya,
                                    'realisasi_jumlah_kuantitas_output': data_extraction.realisasi_jumlah_kuantitas_output,
                                    'target_skp_puslia_notsame':data_extraction.target_biaya != data_extraction.project_skp_id.target_biaya,
                                    'realisasi_skp_puslia_notsame':data_extraction.realisasi_biaya != data_extraction.project_skp_id.realisasi_biaya,
                                    'puslia_sync_date':now,
                                    'is_puslia_sync':True,
                                    'puslia_sync_state':'synced'
                        })
            
            if not data_extraction.project_skp_id.puslia_sync_date:
                task.update({
                    'old_realisasi_biaya':data_extraction.project_skp_id.realisasi_biaya,
                    })
                
            project_skp_pool.write(cr,uid,data_extraction.project_skp_id.id,task,context=None)
            extraction_ids.append(data_extraction.id)
            integration_ids.append(data_extraction.project_skp_puslia_id.id)
        if extraction_ids:
            self.write(cr, uid, extraction_ids, {'state':'synced'}, context=None)      
        if integration_ids:
            data_input_pool.write(cr, uid, integration_ids, {'state':'done'}, context=None)      
            
        return True
    def rollback_sync_with_project_skp(self, cr, uid, ids,context=None):
        project_skp_pool = self.pool.get('project.skp')
        
        now=DateTime.today();
        extraction_ids = []
        integration_ids = []
        for data_extraction in self.browse(cr, uid, ids, context=context):
            task =  {}
            task.update({
                                    'realisasi_biaya':data_extraction.project_skp_id.old_realisasi_biaya,
                                    'realisasi_jumlah_kuantitas_output': data_extraction.project_skp_id.old_realisasi_jumlah_kuantitas_output,
                                    'target_skp_puslia_notsame':False,
                                    'realisasi_skp_puslia_notsame':False,
                                    'puslia_sync_date':None,
                                    'old_target_biaya':None,
                                    'old_realisasi_biaya':None,
                                    'is_puslia_sync':False,
                                    'puslia_sync_state':'not_synced'
                        })
            
            project_skp_pool.write(cr,uid,data_extraction.project_skp_id.id,task,context=None)
            extraction_ids.append(data_extraction.id)
        if extraction_ids:
            self.write(cr, uid, extraction_ids, {'state':'new'}, context=None)      
            
        return True
project_skp_puslia_extraction()

