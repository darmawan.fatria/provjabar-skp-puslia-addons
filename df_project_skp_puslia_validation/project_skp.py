from openerp.osv import fields, osv
from mx import DateTime
import openerp.addons.decimal_precision as dp


class project_skp(osv.Model):
    _inherit = 'project.skp'

    def create(self, cr, uid, vals, context=None):

        if vals.get('project_id', False):
            project_id = vals.get('project_id', False)
            if vals.get('target_type_id', False):
                target_type_id = vals.get('target_type_id', False)
                if target_type_id in ('lain_lain','dpa_opd_biro'):
                    project_obj = self.pool.get('project.project').browse(cr,uid,project_id,context=None)
                    code = project_obj.code
                    kode_instansi = project_obj.kode_instansi
                    vals['code'] = code
                    vals['rekening_code'] = code
                    vals['kode_instansi'] = kode_instansi

        return super(project_skp, self).create(cr, uid, vals, context)

    _columns = {
        'rekening_code' :fields.char('Kode Rekening SIPKD/PUSLIA', size=25),
        'old_realisasi_biaya': fields.float('Realisasi Biaya Yang Diinput Awal', readonly=True),
        'old_realisasi_jumlah_kuantitas_output': fields.float('Realisasi Kuantitas Output Yang Diinput Awal', readonly=True,digits_compute=dp.get_precision('no_digit')),
        'target_skp_puslia_notsame'           : fields.boolean('Target SKP Dan SIPD Tidak Sama',readonly=True),
        'realisasi_skp_puslia_notsame'           : fields.boolean('Realisasi SKP Dan SIPD Tidak Sama',readonly=True),
        'puslia_sync_date'     : fields.date('Tanggal Sinkronisasi Dengan puslia',readonly=True),
        'is_puslia_sync'       : fields.boolean('Status Sinkronisasi puslia'),
        'puslia_sync_state'       : fields.selection([     ('not_synced', 'Tidak Ada Data Sinkronisasi'),
                                                         ('synced', 'Synced'),
                                  ],'Status Realisasi Biaya puslia',),
        'kode_instansi': fields.char('Kode Instansi', size=7),
    }


    def manual_syncronize_puslia_integration(self, cr, uid, ids,context=None):
        data_extraction_pool = self.pool.get('project.skp.puslia.extraction')
        
        for task_id in ids:
            data_extraction_pool.sync_and_update_with_project_skp_by_task_id(cr,uid,task_id,context=None)
        
        return True
    def manual_syncronize_puslia_integration_zero_points(self, cr, uid, ids,context=None):
        
        now=DateTime.today();
        for task_obj in self.browse(cr,uid,ids,context=None):
            task =  {}
            task.update({
                                    'realisasi_biaya':0,
                                    'realisasi_jumlah_kuantitas_output': 0,
                                    'realisasi_skp_puslia_notsame':True,
                                    'puslia_sync_date':now,
                                    'old_realisasi_biaya':task_obj.realisasi_biaya,
                                    'old_realisasi_jumlah_kuantitas_output': task_obj.realisasi_jumlah_kuantitas_output,
                                    'is_puslia_sync':True,
                                    'puslia_sync_state':'not_synced'
                        })
            self.write(cr,uid,task_obj.id,task,context=None) 
           
        
        return True  
    
    def manual_roolback_puslia_integration(self, cr, uid, ids,context=None):
        
        
        for task_obj in self.browse(cr,uid,ids,context=None):
            task =  {}
            task.update({
                                    'realisasi_biaya':task_obj.old_realisasi_biaya,
                                    'realisasi_jumlah_kuantitas_output': task_obj.old_realisasi_jumlah_kuantitas_output,
                                    'realisasi_skp_puslia_notsame':False,
                                    'puslia_sync_date':None,
                                    'old_realisasi_biaya':None,
                                    'old_realisasi_jumlah_kuantitas_output': None,
                                    'is_puslia_sync':False,
                                    'puslia_sync_state':'not_synced'
                        })
            self.write(cr,uid,task_obj.id,task,context=None) 
           
        
        return True  
    
    def action_done(self, cr, uid, ids, context=None):
        """ Selesai Perhitungan
        """
        #get var lock action done ?
        is_lock=False;
        try :
            lock_absen_ids = self.pool.get('config.skp').search(cr, uid, [('code', '=', 'lck_pus'), ('active', '=', True), ('type', '=', 'lain_lain')], context=None)
            is_lock_int  = self.pool.get('config.skp').browse(cr, uid, lock_absen_ids)[0].config_value_int
            if is_lock_int == 1:
                is_lock=True;
            else :
                is_lock=False;
        except :
            is_lock=False;

        if is_lock : 
            duplicate_ids = self.read(cr, uid, ids, ['is_puslia_sync','puslia_sync_date','target_type_id','employee_job_type'], context=context)
            task_ids=[]
            for record in duplicate_ids:
                if record['target_type_id'] == 'lain_lain' :
                    if record['is_puslia_sync'] :
                        task_ids.append(record['id'])
                    else :
                        continue;
                else :
                    task_ids.append(record['id'])
                    
            super(project_skp, self).action_done(cr, uid, task_ids, context=context)
        else :
            super(project_skp, self).action_done(cr, uid, ids, context=context)
            
        return True
    
      
project_skp()