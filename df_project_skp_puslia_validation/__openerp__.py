{
    "name": "Validasi Realisasi SKP : Pendapatan",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Project/SKP",
    "description": "Validasi Realisasi SKP : Pendapatan",
    "website" : "http://www.-",
    "license" : "",
    "depends": ['df_project','df_skp_employee','df_project_consolidation','df_project_skp_sipkd_validation'],
    'data': [
            "security/skp_puslia_security.xml",
            "project_skp_puslia_view.xml",
            "project_view.xml",
            "project_skp_view.xml",
            "app_skp_puslia_settings_view.xml",
             ],
    'installable': True,
    'active': False,
}
