from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools.translate import _

class project(osv.Model):
    _inherit = "project.project"

    def create(self, cr, uid, vals, context=None):

        if vals.get('target_type_id', False):
            target_type_id = vals.get('target_type_id', False)
            if target_type_id == 'lain_lain' :
                code = vals.get('kode_instansi', '') +'.'+ vals.get('kode_pendapatan', '')
                vals['code'] = code
            if target_type_id == 'dpa_opd_biro':
                if vals.get('target_category_id', '') == 'program':
                    code = vals.get('kode_instansi', '') + '.' + vals.get('code_program', '')
                    print "code : ", code
                    vals['code'] = code
                if vals.get('target_category_id', '') == 'kegiatan':
                    code = vals.get('kode_instansi', '') + '.' + vals.get('code_program', '')+ '.' + vals.get('code_kegiatan', '')
                    print "code : ", code
                    vals['code'] = code


        return super(project, self).create(cr, uid, vals, context)

    _columns = {
        'kode_instansi': fields.char('Kode Instansi', size=7),
        'code': fields.char('Kode Kegiatan', size=25, readonly=True, ),
        'kode_pendapatan': fields.char('Kode Pajak/Non Pajak', size=6, readonly=True,
                                    states={'draft': [('readonly', False)], 'new': [('readonly', False)]}, ),

    }
    _defaults = {
        'kode_instansi': lambda self, cr, uid, c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.code,
    }